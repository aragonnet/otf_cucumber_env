FROM maven:3.6.3-openjdk-11-slim

COPY /requirements /tmp/requirements
COPY /root/ /

ARG GECKODRIVER_VER=0.30.0
ENV JAVA_HOME=/usr/local/openjdk-11
ENV DEBIAN_FRONTEND=noninteractive \
    DEBCONF_NONINTERACTIVE_SEEN=true
ARG TZ=Europe/Paris

# 1. Installation of X server,firefox, chromium, chromium-driver and dependancies
RUN apt-get update \
    && apt-get install -qqy --no-install-recommends $(grep -vE "^\s*#" /tmp/requirements/apt-get-requirements-file.txt  | tr "\n" " ") \
    && rm -rf /var/lib/apt/lists/* \

# 2. Create User Squashtf with sudo rights and ssh connections
    && sed -i 's/PasswordAuthentication (yes|no)/#PasswordAuthentication no/g' /etc/ssh/sshd_config \
    && sed -i 's/#PermitUserEnvironment no/PermitUserEnvironment yes/g' /etc/ssh/sshd_config \
    && useradd -m squashtf \
    && echo "squashtf ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
    && mkdir /home/squashtf/.ssh && touch /home/squashtf/.ssh/authorized_keys \
    && chown -R squashtf:squashtf /home/squashtf/ \

# 3. installation of geckodriver (Firefox driver)
    && curl -fL -o /tmp/geckodriver.tar.gz \
        https://github.com/mozilla/geckodriver/releases/download/v${GECKODRIVER_VER}/geckodriver-v${GECKODRIVER_VER}-linux64.tar.gz \
    && tar -xzf /tmp/geckodriver.tar.gz -C /tmp/ \
    && chmod +x /tmp/geckodriver \
    && mv /tmp/geckodriver /usr/local/bin/ \

# 4. Cleanup
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
                $toolDeps \
    && rm -rf /var/lib/apt/lists/* /tmp/* 

# 5. Creation of firefox user in headless mode for X displays
RUN firefox -CreateProfile "headless /squashtf"  -headless

# 6. Ready to boot
USER squashtf
WORKDIR /home/squashtf
EXPOSE 22
ENTRYPOINT bash /init.sh && bash /display.sh && sudo service ssh restart && sleep infinity

